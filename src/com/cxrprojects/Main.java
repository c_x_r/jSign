package com.cxrprojects;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class Main {

    private static final String HMAC_SHA512 = "HmacSHA512";

    private static String STRING_TO_HASH;
    private static String SECRET;

    public static void main(String[] args) throws Exception {
        STRING_TO_HASH = args[0];
        SECRET = args[1];

        String hmac = calculateHMAC(STRING_TO_HASH, SECRET);
        System.out.println(hmac);
    }

    private static String toHexString(byte[] bytes) {
        try (Formatter formatter = new Formatter()) {
            for (byte b : bytes) {
                formatter.format("%02x", b);
            }
            return formatter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String calculateHMAC(String stringToHash, String secret)
            throws NoSuchAlgorithmException, InvalidKeyException
    {
        SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes(), HMAC_SHA512);
        Mac mac = Mac.getInstance(HMAC_SHA512);
        mac.init(secretKeySpec);
        return toHexString(mac.doFinal(stringToHash.getBytes()));
    }

}
